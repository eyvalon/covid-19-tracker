import React from "react";
import { StyleSheet, Text, View, Image, SafeAreaView, ScrollView, Dimensions } from "react-native";

const { height } = Dimensions.get("window");

class Detail extends React.Component<{}> {
  state = {
    screenHeight: 0,
    data: [
      {
        id: 0,
        title: "Fever",
        color: "#FF4500",
        members: 8,
        image: "https://raisingchildren.net.au/__data/assets/image/0030/48549/fever-a.jpg",
        description: "A normal temperature range for children is 36.5°C-38°C. A fever is when your child’s body temperature is higher than 38°C. Fever is not an illness in itself, but is the sign of an illness."
      },
      {
        id: 1,
        title: "Tiredness",
        color: "#23C45B",
        members: 6,
        image: "https://static.independent.co.uk/s3fs-public/thumbnails/image/2018/01/24/16/sleep.jpg?width=668",
        description: "You want nothing more than to return to bed, but more often than not you have the whole day ahead of you. It's the worst. However, it may not just be lack of sleep that's getting to you - the fuel you're putting into your body is just as important."
      },
      {
        id: 2,
        title: "Dry Cough",
        color: "#4682B4",
        members: 12,
        image: "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/just-want-my-bed-royalty-free-image-1585167344.jpg?crop=0.529xw:1.00xh;0.332xw,0&resize=980:*",
        description: "It can feel like any cough without the phlegm. You can also feel like you’re having dryness, a tickle, or tightness in your chest. 'The dry cough people are often experiencing with coronavirus is a very deep, low cough from the bottom of the lungs,'"
      },
      {
        id: 3,
        title: "Aches & Pain",
        color: "#6A5ACD",
        members: 5,
        image: "https://img.webmd.com/dtmcms/live/webmd/consumer_assets/site_images/rich_media_quiz/topic/rmq_aches_pains_facts/getty_rf_photo_of_woman_rubbing_aching_shoulder.jpg?resize=692px",
        description: "Body aches are a common symptom of many conditions. The flu is one of the most well-known conditions that can cause body aches. Aches can also be caused by your everyday life, especially if you stand, walk, or exercise for long periods of time. "
      },
      {
        id: 4,
        title: "Nasal Congesion",
        color: "#FF69B4",
        members: 6,
        image: "https://www.breatheright.com/amp/img/what-is-congestion/breathe-right-what-is-congestion-main.jpg",
        description: "Nasal congestion occurs when the tissues and blood vessels in and around the nose swell up with excess fluid. Sometimes there’s a “discharge” that accompanies congestion (otherwise known as a runny nose), but that’s not always the case."
      },
      {
        id: 5,
        title: "Runny Nose",
        color: "#00BFFF",
        members: 7,
        image: "https://telfast.com.au/wp-content/uploads/2019/05/xGettyImages-903725228.jpg.pagespeed.ic.vsgj7aCwHf.webp",
        description: "A runny nose is sometimes known as “rhinorrhoea” or “rhinitis.” It’s the excess drainage produced by the nose and surrounding blood vessels and can be thin and clear mucus or thick mucus. This excess mucus isn’t just there to annoy you, but traps bacteria and debris from entering your lungs."
      },
      {
        id: 6,
        title: "Sore Throat",
        color: "#0022FF",
        members: 8,
        image: "https://2rdnmg1qbg403gumla1v9i2h-wpengine.netdna-ssl.com/wp-content/uploads/sites/3/2016/01/soreThroat-482481428-770x533-1-745x490.jpg",
        description: "Sore throats are not only a pain, but they can be caused by many different factors. Viruses, bacteria such as Streptococcus pyogenes, dry air, allergies or even drainage from a runny nose can make your throat hurt."
      },
      {
        id: 8,
        title: "Diarrhoea",
        color: "#20B2AA",
        members: 23,
        image: "https://www.doctordoctor.com.au/wp-content/uploads/2018/02/Causes-and-Treatment-of-Diarrhoea-1.jpg",
        description: "Diarrhoea is typically a symptom of a bowel infection which is characterised by loose and watery stools three or more times a day. The condition occurs when the lining of the intestine actively secretes fluids or is unable to absorb fluids."
      }
    ],
  };

  onContentSizeChange = (contentWidth: any, contentHeight: any) => {
    this.setState({ screenHeight: contentHeight });
  };

  render() {
    const scrollEnabled = this.state.screenHeight > height;

    let cardList = [];

    for (let i = 0; i < this.state.data.length; i++) {
      cardList.push(
        <View style={[styles.child, { backgroundColor: this.state.data[i].color }]}>
        <Text style={styles.headerText}>{this.state.data[i].title}</Text>
        <br />
        <Image
          style={styles.imgContainer}
          source={{ uri: this.state.data[i].image}}>
          </Image>
        <Text style={styles.bodyText}>
        { this.state.data[i].description }
        </Text>
    </View>
      );
    }

    return (
      <SafeAreaView style={styles.container}>
        <ScrollView
          scrollEnabled={scrollEnabled}
          onContentSizeChange={this.onContentSizeChange}
        >
        <View style={[styles.parent]}>

          { cardList }

        </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#ebebeb",
  },

  title: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#555555",
  },

  imgContainer: {
    justifyContent: 'center',
    alignItems: "center",
    height: 150,
    marginTop: '10px'
  },

  headerText: {
    fontSize: 20,
    fontWeight: "bold",
    color: "#f8f8f8",
    paddingLeft: 10,
    marginTop: '15px'
  },

  bodyText: {
    fontSize: 14,
    marginLeft: 10,
    marginRight: 10,
    paddingTop: 10,
    color: "#f8f8f8",
    marginTop: '15px'
  },

  parent: {
    width: "100%",
    flexDirection: "row",
    flexWrap: "wrap",
    height:'100vh'

  },
  child: {
    width: "48%",
    margin: "1%",
    aspectRatio: 1,
    height: '450px',
    marginTop: '5px'
  },
});

export default Detail;
