import * as React from "react";
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Dimensions,
  SafeAreaView,
  RefreshControl,
} from "react-native";
import { SearchBar } from "react-native-elements";
import Accordian from "../components/Accordian";

const { height } = Dimensions.get("window");

class Search extends React.Component<{}> {
  state = {
    query: "",
    data: "",
    refreshing: false,
    filteredData: [
      {
        country: null,
        deaths: null,
        cases: null,
        todayCases: null,
        todayDeaths: null,
        recovered: null,
        active: null,
      },
    ],
    arrayholder: [],
    search: "",
    screenHeight: 0,
  };

  _onRefresh = () => {
    this.setState({ refreshing: true });
    this.getData().then(() => {
      this.setState({ refreshing: false });
    });
  };

  onContentSizeChange = (contentWidth: any, contentHeight: any) => {
    this.setState({ screenHeight: contentHeight });
  };

  getData = async () => {
    fetch(`https://coronavirus-19-api.herokuapp.com/countries`)
      .then((response) => response.json())
      .then((data) => {
        const filteredData = data.filter((element: { country: string }) => {
          return element.country.toLowerCase();
        });
        
        this.setState({
          data,
          filteredData,
        });
      });
  };

  async componentWillMount() {
    await this.getData();
  }

  updateSearch = (search: String) => {
    this.setState({ search });

    let filteredData = this.state.data.filter(
      (element: { country: string }) => {
        return element.country.toLowerCase().includes(search.toLowerCase());
      }
    );

    this.setState({ filteredData });
  };

  renderAccordians = () => {
    const items = [];
    for (let item of this.state.filteredData) {
      items.push(
        <Accordian
          cases={item.cases}
          active={item.active}
          recovered={item.recovered}
          country={item.country}
          deaths={item.deaths}
          todayDeaths={item.todayDeaths}
          todayCases={item.todayCases}
        />
      );
    }
    return items;
  };

  render() {
    const scrollEnabled = this.state.screenHeight > height;

    return (
      <SafeAreaView style={styles.container}>
        <SearchBar
          placeholder="Search country here..."
          onChangeText={this.updateSearch}
          autoCorrect={false}
          value={this.state.search}
        />
        <ScrollView
          scrollEnabled={scrollEnabled}
          onContentSizeChange={this.onContentSizeChange}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          <View style={styles.row}>
            <Text style={[styles.title, styles.direction]}>Country</Text>
            <Text style={[styles.title, styles.direction]}>Cases:</Text>
            <Text style={[styles.title, styles.direction]}>
              Today Case Count
            </Text>
          </View>
          {this.renderAccordians()}
        </ScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    backgroundColor: "#ebebeb",
  },
  innerText: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#ebebeb",
  },
  text: {
    color: "#101010",
    fontSize: 24,
    fontWeight: "bold",
  },
  title: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#555555",
  },
  row: {
    flexDirection: "row",
    // justifyContent: "space-between",
    height: 56,
    paddingLeft: 25,
    paddingRight: 18,
    alignItems: "center",
    backgroundColor: "#e9e9e9",
    borderBottomColor: "grey",
  },
  direction: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
  },
});

export default Search;
