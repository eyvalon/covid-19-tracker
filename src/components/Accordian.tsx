import React from "react";
import { View, TouchableOpacity, Text, StyleSheet } from "react-native";

import Icon from "react-native-vector-icons/MaterialIcons";

class Accordian extends React.Component<{}> {
  state = {
    expanded: false,
  };

  render() {
    return (
      <View>
        <TouchableOpacity
          style={styles.row}
          onPress={() => this.toggleExpand()}
        >
          <Text style={[styles.title, styles.direction]}>
            {this.props.country}
          </Text>
          <Text style={[styles.title, styles.direction]}>   {this.props.cases}
          </Text>
          <Text style={[styles.title, styles.direction]}>      {this.props.todayCases}
          </Text>

          <Icon
            name={
              this.state.expanded ? "keyboard-arrow-up" : "keyboard-arrow-down"
            }
            size={30}
            color={"e3e3e3"}
          />
        </TouchableOpacity>
        <View style={styles.parentHr} />
        {this.state.expanded && (
          <View>
            <View style={styles.ctext}>
              <View style={styles.child}>
                <Text>Active: {this.props.active} </Text>
                <Text>Recovered: {this.props.recovered} </Text>
              </View>

              <View style={styles.child}>
                <Text>Death Count: {this.props.deaths} </Text>
                <Text>Today death count: {this.props.todayDeaths}</Text>
              </View>
            </View>
          </View>
        )}
      </View>
    );
  }

  toggleExpand = () => {
    this.setState({ expanded: !this.state.expanded });
  };
}

const styles = StyleSheet.create({
  title: {
    fontSize: 14,
    fontWeight: "bold",
    color: "#555555",
  },
  row: {
    flexDirection: "row",
    // justifyContent: "space-between",
    height: 56,
    paddingLeft: 25,
    paddingRight: 18,
    alignItems: "center",
    backgroundColor: "#e9e9e9",
    borderBottomColor: "grey",
  },
  direction: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-start",
  },
  parentHr: {
    height: 1,
    color: "#ffffff",
    width: "100%",
  },
  ctext: {
    flex: 1,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
  },
  child: {
    alignItems: "center",
    backgroundColor: "e3e3e3",
    padding: 16,
    width: "50%",
  },
});

export default Accordian;
