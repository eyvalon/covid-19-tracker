import * as React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import AnimatedSplash from "react-native-animated-splash-screen";

import Detail from "../screens/Detail";
import Search from "../screens/Search";

const Stack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();

function DetailStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Detail"
        component={Detail}
        options={{
          title: "List of Known Symptoms",
          headerTitleAlign: "center",
        }}
      />
    </Stack.Navigator>
  );
}

function searchStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Detail"
        component={Search}
        options={{
          title: "COVID-19 World Statistic",
          headerTitleAlign: "center",
        }}
      />
    </Stack.Navigator>
  );
}

class MainStackNavigator extends React.Component<{}> {
  state = {
    isLoaded: false,
    filteredData: [
      {
        country: null,
        deaths: null,
        cases: null,
        todayCases: null,
        todayDeaths: null,
        recovered: null,
        active: null,
      },
    ],
  };

  loadAsync() {
    fetch(`https://coronavirus-19-api.herokuapp.com/countries`)
    .then((response) => response.json())
    .then((data) => {
      this.setState({
        isLoaded: true
      });
    });
  }

  async componentDidMount() {
    await this.loadAsync();
    // this.setState({
    //   isLoaded: true
    // });
  }

  render() {
    return (
      <AnimatedSplash
        isLoaded={this.state.isLoaded}
        logoImage={require("../assets/images/covid-19.webp")}
        backgroundColor={"#e2e2e2"}
      >
        <NavigationContainer>
          <Tab.Navigator
            initialRouteName="Symtoms"
            activeColor="#000000"
            inactiveColor="#696969"
            barStyle={{ backgroundColor: "#D3D3D3" }}
          >
            <Tab.Screen
              name="Search"
              component={searchStack}
              options={{ tabBarIcon: "earth", tabBarLabel: "Search" }}
            />
            <Tab.Screen
              name="Symtoms"
              component={DetailStack}
              options={{ tabBarIcon: "hazard-lights", tabBarLabel: "Symptoms" }}
            />
          </Tab.Navigator>
        </NavigationContainer>
      </AnimatedSplash>
    );
  }
}

export default MainStackNavigator;
