## Project setup
```
npm install
```

## Running project

To run on web:
```
yarn run web
```

To run on Android:
```
yarn run android
```

To run on iOS:
```
yarn run ios
```

## Description
Using public API for covid-19 to check for daily measurements of cases, death for the World statistic data. 

This project is created for me to gain an insight towards making the app work for mobile and web integrated.

Public API available from:
```
https://github.com/javieraviles/covidAPI
```
Made using React Native.

## Images
![Alt Text](./video/image.png)
![Alt Text](./video/image1.png)
